import React from "react";
import { Outlet } from "react-router-dom";
import { Navbar, StickyContact } from "../../components";

const LayoutTemplate = () => {
  return (
    <div className="flex flex-col h-screen flex-1">
      <Navbar />
      <StickyContact />
      <Outlet />
    </div>
  );
};

export default LayoutTemplate;
