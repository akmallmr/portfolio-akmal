import React from 'react';

const Experience = () => {
  return (
    <div className="flex flex-1 flex-col items-center bg-white dark:bg-slate-800 dark:text-white h-screen overflow-y-scroll scroll-auto">
      <section className="w-[800px]">
        {/* <div className="sticky top-0 z-10"> */}
        <div className="">
          <h2 className="group-date">Abishar Technologies</h2>
        </div>
        <div className="timeline">
          <div className="relative">
            <div className="dot"></div>
            <div className="pl-10">
              <span className="timeline-date">Dec 2023 - Present</span>
              <h3 className="timeline-title">Software Engineer - Front End</h3>
              <span className="timeline-job-detail">
                Client from Nabati Group
              </span>
              <div className="mt-5 mb-2.5">
                <h2 className="text-xs font-bold">Project:</h2>
                <li className="">eDot Web Application</li>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="w-[800px]">
        {/* <div className="sticky top-0 z-10"> */}
        <div className="">
          <h2 className="group-date">PointStar Indonesia</h2>
        </div>
        <div className="timeline">
          <div className="relative">
            <div className="dot"></div>
            <div className="pl-10">
              <span className="timeline-date">October 2021 - Sept 2023</span>
              <h3 className="timeline-title">Application Developer</h3>
              <span className="timeline-job-detail">
                Second office at Bandung, West Java
              </span>
              <div className="mt-5 mb-2.5">
                <h2 className="text-xs font-bold">Project:</h2>
                <li className="">Veolia Distributor Portal</li>
                <li className="">Customer Portal PointStar</li>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="w-[800px]">
        {/* <div className="sticky top-0 z-10"> */}
        <div className="">
          <h2 className="group-date">PT. Aegis Ultima Teknologi</h2>
        </div>
        <div className="timeline">
          <div className="relative">
            <div className="dot"></div>
            <div className="pl-10">
              <span className="timeline-date">January 2019 - October 2021</span>
              <h3 className="timeline-title">Jr. Software Developer</h3>
              <span className="timeline-job-detail">Bandung, West Java</span>
              <div className="mt-5 mb-2.5">
                <h2 className="text-xs font-bold">Project:</h2>
                <li className="w-1/2 lg:w-full">
                  Mindtrex Academy - Leaderboard and Pre-requesite Exam and
                  Learn Screen
                </li>
                <li className="">Mindtrex Merchant</li>
              </div>
            </div>
          </div>
          <div className="relative">
            <div className="dot"></div>
            <div className="pl-10">
              <span className="timeline-date">October 2019 - January 2021</span>
              <h3 className="timeline-title">Internship</h3>
              <span className="timeline-job-detail">Bandung, West Java</span>
              <div className="mt-5 mb-2.5">
                <h2 className="text-xs font-bold">Project:</h2>
                <li className="">React Native Basic</li>
                <li className="">Fundamental programmer, Javascript basic</li>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="w-[800px]">
        {/* <div className="sticky top-0 z-10"> */}
        <div className="">
          <h2 className="group-date">PT. Multiguna Cipta Sentosa</h2>
        </div>
        <div className="timeline">
          <div className="relative">
            <div className="dot"></div>
            <div className="pl-10">
              <span className="timeline-date">January 2017 - March 2019</span>
              <h3 className="timeline-title">
                Freelance Application Specialist
              </h3>
              <span className="timeline-job-detail">Bandung, West Java</span>
              <div className="mt-5 mb-2.5">
                <h2 className="text-xs font-bold">Project:</h2>
                <li className="">
                  Being product specialist of Rotem from German.
                </li>
              </div>
            </div>
          </div>
          <div className="relative">
            <div className="dot"></div>
            <div className="pl-10">
              <span className="timeline-date">
                September 2017 - January 2019
              </span>
              <h3 className="timeline-title">
                Technician / Applicaton Specialist
              </h3>
              <span className="timeline-job-detail">Bandung, West Java</span>
              <div className="mt-5 mb-2.5">
                <h2 className="text-xs font-bold">Project:</h2>
                <li className="">
                  Being product specialist of Rotem from German.
                </li>
              </div>
            </div>
          </div>
          {/* <div className="relative">
            <div className="dot" title="last item">
              <div className="dot absolute -top-0.5 -left-0.5 animate-ping"></div>
            </div>
          </div> */}
        </div>
      </section>
    </div>
  );
};

export default Experience;
