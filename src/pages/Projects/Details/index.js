import React from "react";
import { useLocation } from "react-router-dom";

import parse from "html-react-parser";

const ProjectDetails = () => {
  const location = useLocation();
  const { data } = location.state;

  return (
    <div className="flex flex-1 flex-col items-center bg-white dark:bg-slate-800 dark:text-white h-screen overflow-y-scroll px-4 lg:-px-4">
      <div className="flex flex-col max-w-3xl w-full h-screen overflow-y-scroll scrollbar-none py-2">
        <div className=" my-3 text-xl">
          <label className="font-bold">
            {data.title1} - {data.title2}{" "}
          </label>
        </div>
        <div className="gap-10 flex flex-col mb-24">
          <div>
            <img src={data.image} className="w-full" alt="image1" />
          </div>
          {data.image2 && (
            <div>
              <img src={data.image2} className="w-full" alt="image2" />
            </div>
          )}
          {data.image3 && (
            <div>
              <img src={data.image3} className="w-full" alt="image3" />
            </div>
          )}
          <div className="flex flex-col gap-2">
            <div>
              <label>Role</label>
              <p className=" font-bold">{data?.detail_project?.role}</p>
            </div>
            <div>
              <label>Skills</label>
              <p className=" font-bold">{data?.detail_project?.skill}</p>
            </div>
            <div>
              <label>Collaboration</label>
              <p className=" font-bold">
                {data?.detail_project?.collaboration}
              </p>
            </div>
            <div>
              <label>Tools</label>
              <p className=" font-bold">{data?.detail_project?.tools}</p>
            </div>
          </div>
          <div>
            <label className=" underline">Description</label>
            {parse(data.detail_project.description_project)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProjectDetails;
