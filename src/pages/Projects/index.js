import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import {
  CustomerPortal1,
  CustomerPortalVers2,
  GoogleSharedDriveExternalLogin,
  MindtrexAcademyProject,
  MindtrexMerchantProject,
  TuvNord,
  VeoliaProject,
} from "../../assets";
import { Card } from "../../components";
import { projectData } from "../../data/projects";

const Projects = () => {
  const navigate = useNavigate();

  useEffect(() => {
    console.log("projectData", projectData);
  }, []);

  return (
    <div className="flex flex-1 flex-col items-center bg-white dark:bg-slate-800 dark:text-white h-screen overflow-y-scroll">
      <div className="flex flex-col max-w-3xl w-full h-screen overflow-y-scroll scrollbar-none gap-3 py-2 px-4 lg:p-0">
        <p>
          Project: {projectData.length === 0 ? "-" : projectData.length} Item(s)
        </p>
        {projectData.map((item) => (
          <Card key={item.id} className={`grid grid-cols-1 lg:grid-cols-3`}>
            <div className="p-5 tracking-widest hidden lg:block">
              <div>
                <h1 className="font-bold text-2xl">{item.title1}</h1>
                <h1 className="ml-5 font-bold text-lg">
                  {item.title2}
                  <span>.</span>
                </h1>
              </div>
              <div className="mt-2">
                <p className="italic text-xs">{item.duration}</p>
              </div>
              <div className="mt-5">
                <p className="text-xs text-justify">{item.description}</p>
              </div>
            </div>
            <div
              className={`col-span-2 ${item.color} justify-center items-center flex cursor-pointer w-full h-full`}
              onClick={() =>
                navigate(`/projects/${item.title1.toLowerCase()}/details`, {
                  state: { data: item },
                })
              }
            >
              {item.category === "google-shared" && (
                // <ILSupermarket width={250} height={250} />
                <img
                  src={GoogleSharedDriveExternalLogin}
                  className=" object-cover"
                  alt="google-shared drive external"
                />
              )}
              {item.category === "merchant" && (
                // <ILSupermarket width={250} height={250} />
                <img
                  src={MindtrexMerchantProject}
                  className=" object-cover"
                  alt="mindtrex merchant"
                />
              )}
              {item.category === "distributorvers2" && (
                // <ILSupermarket width={250} height={250} />
                <img
                  src={CustomerPortalVers2}
                  className=" object-cover"
                  alt="customer portal vers 2"
                />
              )}
              {item.category === "distributor" && (
                // <ILPortalDistributor width={250} height={250} />
                <img
                  src={VeoliaProject}
                  alt="distributor veolia"
                  className=" object-cover"
                />
              )}
              {item.category === "certificate" && (
                // <ILCertificate width={250} height={250} />
                <img src={TuvNord} alt="tuvnord" className=" object-cover" />
              )}
              {item.category === "academy" && (
                // <ILEducation width={250} height={250} />
                <img
                  src={MindtrexAcademyProject}
                  alt="mindtrex academy"
                  className=" object-cover"
                />
              )}
              {item.category === "b2b" && (
                // <ILProduction width={250} height={250} />
                <img
                  src={CustomerPortal1}
                  alt="internal: customer portal"
                  className=" object-cover"
                />
              )}
            </div>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default Projects;
