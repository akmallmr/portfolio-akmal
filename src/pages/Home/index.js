import React from "react";
import { Text } from "../../components";

import { OwnProfilePicture } from "../../assets";

const Home = () => {
  return (
    <div className="flex flex-1 flex-col justify-center items-center bg-white dark:bg-slate-800 dark:text-white h-screen">
      <div className="flex flex-col justify-center items-center max-w-3xl w-full">
        <div className="mb-8">
          <div className=" bg-gray-200 p-1 rounded-full resize-none w-48 h-48">
            <img
              src={OwnProfilePicture}
              className=" rounded-full object-contain"
              alt="profile"
            />
          </div>
        </div>
        <div className=" text-center container max-w-md lg:max-w-full">
          <div className="mb-5">
            <Text className={`text-3xl`}>
              Hi, I'm{" "}
              <span className="text-green-secondary dark:text-green-tertiary font-bold">
                Akmal!
              </span>
            </Text>
          </div>
          <div className="mb-4 text-sm">
            <Text>
              I'm a front-end developer who graduated from Electrical
              Engineering with special expertise in Biomedical Engineer,
              Healthcare devices. I like the world of{" "}
              <span className="text-green-secondary dark:text-green-tertiary font-bold">
                technology
              </span>{" "}
              and also{" "}
              <span className="text-green-secondary dark:text-green-tertiary font-bold">
                visual
              </span>{" "}
              things. And becoming a{" "}
              <span className="text-green-secondary dark:text-green-tertiary font-bold">
                creator
              </span>{" "}
              is my dream. So 2019, I switched careers to front-end developer
              (React & React Native).
            </Text>
          </div>
          <div className="text-sm">
            <Text>
              Having a career like now makes me enjoy. Slicing UI, positioning
              components for UX purposes and selecting colors is the visual
              plane I want. Amidst my busy life at PointStar, now I need a place
              to collaborate with my experiences. And open a freelance to
              explore more about Front-end Developer.
            </Text>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
