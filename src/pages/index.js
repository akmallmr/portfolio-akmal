import Home from "./Home";
import Experience from "./Experience";
import Projects from "./Projects";
import ProjectDetails from "./Projects/Details";
import LayoutTemplate from "./LayoutTemplate";

import ErrorPage from "./Error";

export {
  Home,
  Experience,
  Projects,
  ErrorPage,
  LayoutTemplate,
  ProjectDetails,
};
