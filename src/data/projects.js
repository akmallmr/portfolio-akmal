import {
  CustomerPortal1,
  CustomerPortal2,
  CustomerPortal3,
  CustomerPortalVers2,
  GoogleSharedDriveExternalLogin,
  MindtrexAcademyProject,
  MindtrexMerchantProject,
  TuvNord,
  VeoliaProject,
  VeoliaProjectTranslate,
} from "../assets";

export const projectData = [
  {
    id: 0,
    title1: "Google Shared",
    title2: "Drive External",
    duration: "August 2023 - Sept 2023",
    description:
      "Making an apps like google drive just for enterprise domain, only to see the permission list, which accounts (public) can access files from the enterprise user. Also this apps can update the permission too.",
    color: "bg-blue-200",
    category: "google-shared",
    image: GoogleSharedDriveExternalLogin,
    image2: null,
    detail_project: {
      role: "Front End Developer",
      skill: "Slicing UI, Collaboration, Google OAuth. state management",
      collaboration: "Backend Developer (Manager).",
      tools: "React js, Redux toolkit, TailwindCSS, Google API.",
      description_project:
        "<p className=\"text-justify\">Google Shared Drive Eksternal is the apps that help the user to know the permissions each file in drive for enterprise level. Because in the original ones, Google Drive haven't a features to see how many user that have an access to this files.<br><br>With this tools, we can see the user list for admin roles and drive list (files list). Also in here, we can edit or update the permissions, so the user don't need go trough the original google drive to change it. And PointStar will make a subscription for this soon.</p>",
    },
  },
  {
    id: 1,
    title1: "Pointstar",
    title2: "Customer Portal Vers. 2",
    duration: "May 2023 - August 2023",
    description:
      "Renew from vers. 1 and starting develop iOS vers. with React Native v0.72 and redux toolkit. Also we added payment method, stripe, implemented in server side.",
    color: "bg-yellow-200",
    category: "distributorvers2",
    image: CustomerPortalVers2,
    detail_project: {
      role: "Front End Developer",
      skill: "Slicing UI, Collaboration, Logic.",
      collaboration: "Worked with UI/UX, Backend Developer (Manager).",
      tools: "React Native v0.72, Redux toolkit, Stripe (Server Side).",
      description_project:
        '<p className="text-justify"> New version of Customer Portal, building Android and iOS Apps using React Native 0.72. For this version, I implemented a payment using stripe, Front End just hit API to the backend thats already installed stripe. We have 3 method payment, Bank transfer, QRIS (PayNow) for SG, and credit card. THis payment method only available for additional licences for a customer who want to buy a new licences one.</p>',
    },
  },
  {
    id: 2,
    title1: "Veolia",
    title2: "Distributor Portal",
    duration: "August 2022 - May 2023",
    description:
      "Make responsive CMS to connecting Veolia company to their distributor. Also we add some features like translate google API and GCP product.",
    color: "bg-purple-200",
    category: "distributor",
    image: VeoliaProject,
    image2: VeoliaProjectTranslate,
    detail_project: {
      role: "Front End Developer",
      skill: "Slicing UI, Collaboration, Logic.",
      collaboration: "Worked with UI/UX, Backend Developer (Manager).",
      tools:
        "React js, Redux toolkit (only translation feature), TailwindCSS, gitlab.",
      description_project:
        '<p className="text-justify"> Veolia Distributor Portal is the Content Management System (CMS) for Veolia company to integrate and deliver their business platform to their distributor. With this apps, veolia can control, manage, and monitoring veolia business. This apps have 3 role access, which are admin, manager and distributor.<br /><br />We also make a translate features for China mainland distributor and have been integration to the redux toolkit. This one is our challenge, how do we make an application using Google platform. However, it can also be used for chinese distributors, considering that China has blocked Google products.</p>',
    },
  },
  {
    id: 3,
    title1: "Pointstar",
    title2: "Customer Portal Vers. 1",
    duration: "Oktober 2021 - April 2022",
    description:
      "Pointstar Android Apps for connecting their client who need to extends their license of pointstar product. This apps using React Native and redux for manage the state. Also this apps is responsive for any Android screen types.",
    color: "bg-red-200",
    category: "b2b",
    image: CustomerPortal1,
    image2: CustomerPortal2,
    image3: CustomerPortal3,
    detail_project: {
      role: "Front End Developer",
      skill: "Slicing UI, Collaboration, Logic, State Management",
      collaboration: "Worked with UI/UX, Backend Developer (Manager).",
      tools: "React Native v64, Redux, gitlab.",
      description_project:
        '<p className="text-justify">  My responsibility are making Andorid Apps, called Customer Portal using React Native. This apps is to integrating our customer for create, extends / renewal, claim, upgrade licence and request training for the customer. Also in here we shared our content and delivered to our customer for their insight. <br /><br /> For our newest version, soon, we want to adding some feature payment on it. So customer will be easier to finishing their closing. Also we need to upgrade the core version RN from 0.64 to the newest version.</p>',
    },
  },
  {
    id: 4,
    title1: "Tuv-Nord SIM Kal",
    title2: "Indonesia",
    duration: "Mei 2021 - September 2021",
    description:
      "Tuv-nord has a plan to migrate their internal apps with React and Golang to improve optimization. This apps using theme forest (Core UI), redux for state management and Golang for backend. With this apps, clients will recieve the information about calibration of their factory machines.",
    color: "bg-yellow-600",
    category: "certificate",
    image: TuvNord,
    detail_project: {
      role: "Front End Developer",
      skill: "Slicing UI, Collaboration",
      collaboration:
        "Make some mock up before our clients deals. Collaboration with manager / CEO.",
      tools: "React js, React Native TailwindCSS, gitlab.",
      description_project:
        '<p className="text-justify"> Tuv Nord, a company from German that recieve calibration services for factory machines with standard international. They have an internal apps called SIM Kal to connect their client, and integrating any database and information about clients machines with automation. In here, Tuv Nord have some database of client\'s factory machines and inform the client about the calibration expiration date. <br /><br />But unfortunately this project cancelled after we give them the budget plan.</p>',
    },
  },
  {
    id: 5,
    title1: "Mindtrex",
    title2: "Academy",
    duration: "February 2021 - Mei 2021",
    description:
      "I'm involved in enhancing in leaderboard screen and make pre-requesite features in every courses (Exam and Learn).",
    color: "bg-orange-200",
    category: "academy",
    image: MindtrexAcademyProject,
    detail_project: {
      role: "Front End Developer",
      skill: "Slicing UI, Collaboration, Logic.",
      collaboration: "Backend Developer",
      tools: "React Native, Redux, gitlab.",
      description_project:
        '<p className="text-justify">Mindtrex Academy who is in collaboration with the minister of education, is a platform education from Brunei Darussalam for elementary school. I involved in this proejct to enhanced some components in leaderboards also add some feature pre-requesite for all of the courses.<br /><br /> So, when the student wants to do learn / exam in the higher level, the student must pass the lower level first. And also when you want to get the prize / voucher.</p>',
    },
  },
  {
    id: 6,
    title1: "Mindtrex",
    title2: "Merchant",
    duration: "April 2020 - Mei 2021",
    description:
      "Responsible to makes mindtrex merchant android apps using React Native. With redux for the state management and adding QR Code Scanner features to validate the discount.",
    color: "bg-teal-200",
    category: "merchant",
    image: MindtrexMerchantProject,
    detail_project: {
      role: "Front End Developer",
      skill: "Slicing UI, Collaboration, Logic.",
      collaboration: "Worked with UI/UX, Backend Developer.",
      tools: "React Native, Redux, gitlab.",
      description_project:
        '<p className="text-justify"> Mindtrex merchant is complementary application from mindtrex academy. This merchants apps is for markets who collaboration with the mindtrex academy. So their product will have a discount only for the students who subscribes mindtrex academy.<br /><br />After add their product to the database, the QR code will be generated instantly into the coupon that will distributed to the students who finished the courses and the exam. They will receive the coupon based on grades. Once they want to exchanges the coupon, the markets just scan the QR codes by their smart phones camera, and the validation will do the rest.</p>',
    },
  },
];
