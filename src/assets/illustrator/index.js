import { ReactComponent as ILLayout } from "./scattered-forcefields.svg";
import { ReactComponent as ILSupermarket } from "./undraw_shopping.svg";
import { ReactComponent as ILPortalDistributor } from "./portal_distributor.svg";
import { ReactComponent as ILCertificate } from "./undraw_certificate.svg";
import { ReactComponent as ILEducation } from "./undraw_education.svg";
import { ReactComponent as ILProduction } from "./undraw_product.svg";

export {
  ILLayout,
  ILSupermarket,
  ILPortalDistributor,
  ILCertificate,
  ILEducation,
  ILProduction,
};
