import OwnProfilePicture from "./akmal-profpic.jpeg";
import GIFConnect from "./icons8-connect.gif";
import Logo from "./logo.png";

import VeoliaProject from "./veolia-distributor-portal.png";
import VeoliaProjectTranslate from "./veolia-translation.png";
import MindtrexAcademyProject from "./mindtrex-academy.png";
import MindtrexMerchantProject from "./mindtrex-merchant-project.png";

import TuvNord from "./tuvnord-web-mobile.png";

import CustomerPortal1 from "./customer-portal-1.png";
import CustomerPortal2 from "./customer-portal-2.png";
import CustomerPortal3 from "./customer-portal-3.png";
import CustomerPortalVers2 from "./customer-portal-vers-2.png";

import GoogleSharedDriveExternalLogin from "./google-shared-drive-external.png";

import { ReactComponent as SVGGmail } from "./icons8-gmail-logo.svg";
import { ReactComponent as SVGInstagram } from "./icons8-instagram.svg";
import { ReactComponent as SVGLinkedin } from "./icons8-linkedin-2.svg";

export {
  OwnProfilePicture,
  GIFConnect,
  SVGGmail,
  SVGInstagram,
  SVGLinkedin,
  Logo,
  VeoliaProject,
  VeoliaProjectTranslate,
  CustomerPortal1,
  CustomerPortal2,
  CustomerPortal3,
  CustomerPortalVers2,
  MindtrexAcademyProject,
  MindtrexMerchantProject,
  TuvNord,
  GoogleSharedDriveExternalLogin,
};
