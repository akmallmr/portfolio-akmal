import { HashRouter, Route, Routes } from "react-router-dom";
import {
  Projects,
  ErrorPage,
  Home,
  LayoutTemplate,
  Experience,
  ProjectDetails,
} from "./pages";

function App() {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" element={<LayoutTemplate />}>
          <Route path="/" element={<Home />} />
          <Route path="projects" element={<Projects />} />
          <Route path="projects/:id/details" element={<ProjectDetails />} />
          <Route path="experience" element={<Experience />} />
          <Route path="*" element={<ErrorPage />} />
        </Route>
      </Routes>
    </HashRouter>
  );
}

export default App;
