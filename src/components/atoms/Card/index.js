import React from "react";

const Card = ({ children, onClick, className }) => {
  return (
    <div
      className={`bg-white dark:bg-slate-700 dark:border-slate-700  h-auto border shadow-lg ${className}`}
      onClick={onClick}
    >
      {children}
    </div>
  );
};

export default Card;
