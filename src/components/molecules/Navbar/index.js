import React, { useEffect, useState } from 'react';
import { FaMoon, FaSun, FaBars } from 'react-icons/fa';
import { Link, useNavigate } from 'react-router-dom';
import { Text } from '../../../components';

const Navbar = () => {
  const activeFade = `transition-opacity opacity-100 duration-300`;
  const passiveFade = `transition-opacity opacity-0 duration-300`;
  const navigate = useNavigate();
  const [theme, setTheme] = useState(null);
  const [fade, setFade] = useState(false);

  const [showNavbar, setShowNavbar] = useState(false);

  const MenuNavbar = [
    { id: 0, title: 'Home', path: '/' },
    { id: 1, title: 'Experience', path: '/experience' },
    { id: 2, title: 'Project', path: '/projects' },
  ];

  useEffect(() => {
    if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
      setTheme('dark');
    } else {
      setTheme('light');
    }
  }, []);

  useEffect(() => {
    if (theme === 'dark') {
      document.documentElement.classList.add('dark');
    } else {
      document.documentElement.classList.remove('dark');
    }
  }, [theme]);

  const handleThemeSwitch = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark');
    setFade(!fade);
  };

  return (
    <div
      className={`shadow-md ${
        theme === 'dark' ? 'border-0' : 'border-b-2'
      } top-0 left-0 bg-white dark:bg-slate-700 flex w-full items-center justify-center py-4`}
    >
      <div className="max-w-3xl flex w-full items-center px-4 md:p-0">
        <div className="flex-1 cursor-pointer" onClick={() => navigate('/')}>
          <h1 className="font-bold tracking-widest dark:text-white">
            akmalmrsyd
          </h1>
          <Text className={`dark:text-white text-xs`}>
            Status: <span className=" text-green-400">Employeed</span> at
            Abishar Technologies
          </Text>
          {/* <Text className={`dark:text-white text-xs`}>Status: <span className=" text-red-400">Unemployed</span></Text> */}
        </div>
        <div className="relative flex lg:hidden">
          <FaBars
            size={25}
            className={`fill-white cursor-pointer`}
            onClick={() => setShowNavbar(!showNavbar)}
          />
          {/* MODAL POPUP FROM HAMBURGER */}
          {showNavbar && (
            <div
              className={`${
                showNavbar &&
                'absolute right-0 top-10 w-52 h-auto border rounded-lg dark:bg-slate-700 bg-white z-999999 py-3'
              }`}
            >
              <div className="items-center justify-center flex flex-col gap-3">
                {MenuNavbar.map((item, _) => (
                  <Link
                    to={item.path}
                    key={_}
                    className="cursor-pointer w-24 text-center dark:text-white"
                  >
                    <p className="text-xl hover:font-bold">{item.title}</p>
                  </Link>
                ))}
                <div
                  className={` flex justify-center items-center  rounded-lg cursor-pointer`}
                >
                  {theme === 'dark' ? (
                    <div
                      className={`flex items-center gap-2 dark:text-white ${
                        theme === 'dark'
                          ? 'hover:bg-gray-500'
                          : 'hover:bg-gray-200'
                      }`}
                      onClick={handleThemeSwitch}
                    >
                      <FaSun
                        size={25}
                        className={`fill-yellow-300 transition-opacity ${
                          theme === 'dark' ? activeFade : passiveFade
                        }`}
                      />
                      <p>Light</p>
                    </div>
                  ) : (
                    <div
                      className="flex items-center gap-2 dark:text-white"
                      onClick={handleThemeSwitch}
                    >
                      <FaMoon
                        size={25}
                        className={`fill-slate-800 ${
                          theme === 'dark' ? passiveFade : activeFade
                        }`}
                      />
                      <p className="">Dark</p>
                    </div>
                  )}
                </div>
              </div>
            </div>
          )}
          {/*END OF MODAL POPUP FROM HAMBURGER */}
        </div>
        {/* TABLET AND WINDOWS */}
        <div className="justify-center hidden lg:flex">
          <div className="items-center justify-center flex">
            {MenuNavbar.map((item, _) => (
              <Link
                to={item.path}
                key={_}
                className="cursor-pointer w-24 text-center dark:text-white"
              >
                <p className="text-sm hover:font-bold">{item.title}</p>
              </Link>
            ))}
          </div>
          <div
            className={`w-10 h-10 flex justify-center items-center ${
              theme === 'dark' ? 'hover:bg-gray-500' : 'hover:bg-gray-200'
            } rounded-lg cursor-pointer`}
          >
            {theme === 'dark' ? (
              <FaSun
                onClick={handleThemeSwitch}
                size={25}
                className={`fill-yellow-300 transition-opacity ${
                  theme === 'dark' ? activeFade : passiveFade
                }`}
              />
            ) : (
              <FaMoon
                onClick={handleThemeSwitch}
                size={25}
                className={`fill-slate-800 ${
                  theme === 'dark' ? passiveFade : activeFade
                }`}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
