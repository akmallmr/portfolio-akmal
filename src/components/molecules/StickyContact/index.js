import React from "react";
import { Link } from "react-router-dom";
import {
  GIFConnect,
  SVGGmail,
  SVGInstagram,
  SVGLinkedin,
} from "../../../assets";

const StickyContact = () => {
  return (
    <div className="flex items-center">
      <div className="absolute w-36 h-14 -left-28 px-1 hover:-left-0.5 hover:justify-end transition-all duration-500 bg-white dark:bg-green-tertiary rounded-r-lg shadow-md justify-center items-center flex top-[50%] border-2">
        <div className="flex items-center gap-2 mr-4">
          <Link
            to="#"
            target={"_blank"}
            rel="noopener noreferrer"
            // className=" hover:bg-slate-400 p-1 h-7 flex justify-center items-center rounded-full"
            onClick={(e) => {
              window.open("mailto:akmallmr@gmail.com", "_blank");
              e.preventDefault();
            }}
          >
            <SVGGmail className="w-5 cursor-pointer" />
          </Link>
          <Link
            to={"//www.instagram.com/akmalmrsyd"}
            target="_blank"
            // className=" hover:bg-slate-400 p-1 h-7 flex justify-center items-center rounded-full"
          >
            <SVGInstagram className="w-5 cursor-pointer " />
          </Link>
          <Link
            to={"//www.linkedin.com/in/akmalmr/"}
            target="_blank"
            // className=" hover:bg-slate-400 p-1 h-7 flex justify-center items-center rounded-full"
          >
            <SVGLinkedin className="w-5 cursor-pointer" />
          </Link>
        </div>
        <div>
          <img src={GIFConnect} className="w-5 ml-2" alt="connect" />
        </div>
      </div>
    </div>
  );
};

export default StickyContact;
