module.exports = {
  content: ["./src/**/*.{html,js}"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        green: {
          primary: "#395144",
          secondary: "#4E6C50",
          tertiary: "#CFFF8D",
        },
        brown: {
          primary: "#AA8B56",
          secondary: "#F0EBCE",
        },
      },
    },
  },
  plugins: [
    require("tailwind-scrollbar-hide"),
  ],
};
